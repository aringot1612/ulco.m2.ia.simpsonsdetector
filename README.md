# ulco.m2.ia.simpsonsDetector

Projet étudiant M2 : IA - Simpsons Detector

## Auteur
- [Ringot Arthur](https://gitlab.com/aringot1612)

## Requirement

[The Simpsons Characters Data (full)](https://www.kaggle.com/alexattia/the-simpsons-characters-dataset/version/4?select=simpsons_dataset)